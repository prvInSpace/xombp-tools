

import requests

class Neocities():

    def __init__(self, api_key) -> None:
        if not api_key:
            raise Exception("API key is empty")
        self.__api_key = api_key

    def __get_header_data(self) -> dict[str, str]:
        return {"Authorization": f"Bearer {self.__api_key.strip()}"}

    def file_exists(self, file: str, path: str = None) -> bool:
        for f in self.get_file_list(path):
            if f['path'] == file:
                return True
        return False

    def get_file_list(self, path="") -> list:
        data = requests.get(
            "https://neocities.org/api/list",
            params={"path": path} if path else {},
            headers=self.__get_header_data()
        ).json()
        return data['files']

    def upload(self, path, file_on_disk) -> None:
        with open(file_on_disk, 'rb') as file:
            data = {path: file}
            message = requests.post(
                "https://neocities.org/api/upload",
                files=data,
                headers=self.__get_header_data()
            )
            if message.status_code != 200:
                print(message.json()['message'])
            else:
                print("Upload successful!")

        

