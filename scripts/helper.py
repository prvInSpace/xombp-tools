

import datetime
import string

def get_date_string() -> str:
    now = datetime.datetime.now()
    return now.strftime("%Y %B %d")


def hex_to_rgb(hex: str) -> dict[str, str]:
    hex = hex.lstrip("#")
    r, g, b = (
        int(hex[i:i+2], 16) / 255
        for i in [0, 2, 4]
    )
    return {"red": r, "green": g, "blue": b}


def col_num_to_letter(n,b=string.ascii_uppercase):
   d, m = divmod(n,len(b))
   return col_num_to_letter(d-1,b)+b[m] if d else b[m]
