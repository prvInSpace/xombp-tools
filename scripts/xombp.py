
from dataclasses import dataclass
from enum import Enum
import gspread
import pandas as pd
from helper import col_num_to_letter, get_date_string, hex_to_rgb
from gspread.utils import ValueInputOption


class House(Enum):
    COMMONS = {
        "color": "#006547",
        "sheet": 1293177126,
        "channel": ''
    }
    LORDS = {
        "color": "#b41d44",
        "sheet": 1461303176,
        "channel": ''
    }

PARTY_TO_ICON = {
    "Independent": "independent",
    "Liberal Democrats": "libdems",
    "Labour": "labour"
}

PARTY_TO_EMOTE = {
    "Independent": "<:independent:556471071902138368>",
    "Liberal Democrats": "<:liberals:848929243361706036>",
    "Labour": "<:labour:640296249878118440>"
}

PARTY_TO_COLOUR = {
    "Labour": {
        'bg': "#d11428",
        'text': "#ffffff"
    }
}

class XOMBPData:

    def __init__(self) -> None:
        gc = gspread.service_account(filename='keys/gsheets.json')
        self.__spreadsheet = gc.open_by_key('1IQxNJkf9ECDAOIsUUNGnZXAMseLtOsbRzqg7RIHNfcU')
        self.__characters = None
    
    def __get_spreadsheet(self, name: str, lines_to_drop: int = 1) -> pd.DataFrame():
        """Converts a worksheet to a Pandas DataFrame.

        Returns:
            DataFrame: A DataFrame containing the data of the worksheet 
        """
        ws = self.__spreadsheet.worksheet(name)
        data = ws.get_all_values()[lines_to_drop:]
        columns = data.pop(0)
        data = filter(
            lambda x: not "".join(x).isspace(),
            data 
        )
        return pd.DataFrame(
            data,
            columns=map(lambda x: x.replace(' ', '_'), columns),
        )

    def get_bills(self) -> pd.DateOffset:
        ws = self.__spreadsheet.get_worksheet_by_id(806386324)
        data = ws.get_values("A2:D")
        columns = data.pop(0)
        columns = map(lambda x: x.replace('.', ''), columns)
        return pd.DataFrame(
            data,
            columns=map(lambda x: x.replace(' ', '_'), columns),
        )

    def get_characters(self) -> pd.DataFrame:
        if self.__characters is None:
            self.__characters = self.__get_spreadsheet('Characters')
        return self.__characters

    def get_members(self, house: House) -> pd.DataFrame:
        ws = self.__spreadsheet.get_worksheet_by_id(house.value['sheet'])
        data = ws.get_values("C2:E")
        data.pop(1)
        columns = data.pop(0)
        while not data[-1][0]:
            data.pop()
        return pd.DataFrame(
            data=data,
            columns=columns
        )
        
    def add_votes(self, house: House, title: str, division: str, votes: list, speaker: str) -> None:
        ws = self.__spreadsheet.get_worksheet_by_id(house.value['sheet'])
        col = col_num_to_letter(ws.col_count)
        row = ws.row_count

        aye = "AYE" if house == House.COMMONS else "CON"
        abs = "ABS" if house == House.COMMONS else "PRE"
        no = "NO" if house == House.COMMONS else "NOT"

        # Insert the new column
        ws.insert_cols(
            [['', title, division, *votes, "", speaker,
                f'=COUNTIF({col}$4:{col}${len(votes)+5}, "{aye}")',
                f'=COUNTIF({col}$4:{col}${len(votes)+5}, "{no}")',
                f'=COUNTIF({col}$4:{col}${len(votes)+5}, "{abs}")',
                f'=COUNTIF({col}$4:{col}${len(votes)+5}, "DNV")',
                f'=IF({col}${row-6} > {col}${row-5}, "{aye}", IF({col}${row-5} > {col}${row-6}, "{no}", "N/A"))',
                f'={col}${row-6}+{col}${row-5}+{col}${row-4}',
                f'=IFERROR({col}${row-1} / ({col}${row-1} + {col}${row-3}), "N/A")'
            ]],
            col=ws.col_count+1,
            inherit_from_before=True,
            value_input_option=ValueInputOption.user_entered
        )
        # Merge cells if there is no division title
        if not division:
            ws.merge_cells(f"{col}2:{col}3", merge_type="MERGE_ALL")
        border = {
            'style': "SOLID"
        }
        ws.format(f"{col}4:{col}",{
            'borders': {
                'top': border,
                'bottom': border,
                'left': border,
                'right': border
            }
        })

        # Add the bold lines for the parties
        party_names = ws.get_values("A4:B")
        for i, row in enumerate(party_names):
            if row[0] or row[1]:
                ws.format(f"{col}{4+i}",{
                    'borders': {
                        'top': { "style": "SOLID_MEDIUM" },
                        'bottom': { "style": "SOLID" },
                        'left': { "style": "SOLID" },
                        'right': { "style": "SOLID" }
                    }
                })

    def add_bill(self, id: str, title: str, author: str, party: str) -> None:
        ws = self.__spreadsheet.get_worksheet_by_id(806386324)
        ws.get_all_values()
        ws.insert_row(
            [id,
                title, 
                "2nd Reading", 
                author, 
                "C" if id.startswith("C") else "L",
                get_date_string(),
                "-", "", "",
                "L" if id.startswith("C") else "C",
                "", "", "","",
                id[0], "", "L" if id.startswith("C") else "C"
            ],
            index = ws.row_count + 1,
            value_input_option=ValueInputOption.user_entered,
            inherit_from_before=True
        )

        color = PARTY_TO_COLOUR.get(party, {
            'bg': "#a29eb6",
            'text': "#000000"
        })

        border = {
            'style': "SOLID"
        }
        houseColor = "#006547" if id.startswith("C") else "#b41d44"
        textFormat = {
                "fontFamily": "Roboto Condensed",
                "fontSize": 10 ,
                "foregroundColor": hex_to_rgb("#ffffff")
        }

        # Format the whole line
        ws.format(f"A{ws.row_count+1}:{ws.row_count+1}", {
            'textFormat': textFormat,
            'borders': {
                'top': border,
                'bottom': border,
                'left': border,
                'right': border
            }
        })

        # Format the author field
        ws.format(f"D{ws.row_count+1}", {
            "backgroundColor": hex_to_rgb(color['bg']),
            'textFormat': { **textFormat, 
                "foregroundColor": hex_to_rgb(color['text'])
            }
        })

        # Color house fields
        ws.format(f"A{ws.row_count+1}", {
            "backgroundColor": hex_to_rgb(houseColor)
        })
        ws.format(f"C{ws.row_count+1}", {
            "backgroundColor": hex_to_rgb(houseColor)
        })
        ws.format(f"G{ws.row_count+1}", {
            'textFormat': {**textFormat,  'foregroundColor': hex_to_rgb("#000000")}
        })

        # Format the link
        ws.format(f"B{ws.row_count+1}", {
            "textFormat": {**textFormat,
                'foregroundColor': hex_to_rgb("#000000"),
                'underline': True,
                "link": {
                    "uri": f"https://xombp.neocities.org/legislation/{id.lower()}.html"
                }
            }
        })

    def fix_b_column(self):
        textFormat = {
                "fontFamily": "Roboto Condensed",
                "fontSize": 10 ,
                "foregroundColor": hex_to_rgb("#ffffff")
        }
        ws = self.__spreadsheet.get_worksheet_by_id(806386324)
        for i in range(3, ws.row_count+1):
            id = ws.get_values(f"A{i}")[0][0].lower()
            ws.format(f"B{i}", {
                "textFormat": {**textFormat,
                    'foregroundColor': hex_to_rgb("#000000"),
                    'underline': True,
                    "link": {
                        "uri": f"https://xombp.neocities.org/legislation/{id}.html"
                    }
                }
            })


