

from xombp import House, XOMBPData
import interactions
from interactions import autodefer
import discord.utils
import urllib
import numpy as np
import re

TOKEN = "MTAxNzA5MzQ0MzMxMjQyMjk0Mw.G2uR4V.1EFTMUulBsgrTRq8EixWilbCQAhi-P_oR4QO9k"
#client = discord.Client(intents=discord.Intents.default())
#slash = SlashCommand(client, sync_commands=True)

bot = interactions.Client(
    token=TOKEN
)

data = XOMBPData()


@bot.command(
    name="fullname",
    description="Returns a the fullname of the character if it exists",
    options= [
        interactions.Option(
            name="name",
            description="The name to search for",
            type=interactions.OptionType.STRING,
            required=True
        )
    ]
)
@autodefer()
async def fullname(ctx: interactions.CommandContext, name: str):
    chars = data.get_characters()
    char = chars.loc[chars['Name'].str.contains(name, case=False)]
    if len(char) == 0:
        await ctx.send("Unable to find a character with that name")
    if len(char) > 1:
        await ctx.send("Found more than one character with that name")
    if len(char) == 1:
        await ctx.send(char.iloc[0]['Full-name'])


@bot.command(
    name="tally",
    description="Tallies the votes for a division and ads it to the spreadsheet.",
    options= [
        interactions.Option(
            name="msg",
            description="The message to extract the votes from",
            type=interactions.OptionType.STRING,
            required=True
        ),
        interactions.Option(
            name="id",
            description="The ID of the bill. Only required if it can't be determined from the message",
            type=interactions.OptionType.STRING,
            required=False
        ),
        interactions.Option(
            name="division",
            description="The name of the division. Only required if it can't be determined from the message",
            type=interactions.OptionType.STRING,
            required=False
        )
    ]
)
async def tally(ctx: interactions.CommandContext, msg: int, id: str = None, division: str = None):

    # Check if user is a deputy speaker
    if not 556489506765668362 in ctx.author.roles:
        await ctx.send("User not authorised to perform that actions.")
        return

    # Defer because this takes a bit to process
    await ctx.defer()
    
    # Find the message
    is_lords = False
    message = await get_message(ctx, "commons-division", msg)
    if message is None:
        message = await get_message(ctx, "lords-division", msg)
        is_lords = True
    if message is None:
        await ctx.send("Message was not found")
        return

    # Determine the Bill / Motion ID
    if id is None:
        id = re.search(r"[CL][BM][0-9]{1,2}-[0-9]{1,3}", message.content, re.IGNORECASE)
        if not id:
            await ctx.send("Unable to determine the id of the bill. Please use the id parameter to specify it manually")
        id = id.group()

    # Determine the title of the division
    if division is None:
        if id[1] == "M":
            division = ""
        if re.search(r"(2nd|second reading)", message.content, re.IGNORECASE):
            division = "2nd"
        elif re.search(r"(3rd|third reading)", message.content, re.IGNORECASE):
            division = "3rd"
        elif (m := re.search(r"(?<=Amendment )[0-9A-Z]+(?=[* ])", message.content, re.IGNORECASE)):
            division = "Amdt " + m.group()
        else:
            await ctx.send("Division title (reading) not found. Please use the division parameter to specify it manually.")
            return

    # Attempt to fetch the bill name
    data = XOMBPData()
    bills = data.get_bills()
    bill = bills[bills["Bill_No"] == id.upper()]
    if len(bill) == 0:
        await ctx.send("Unable to fetch the title of the bill from the spreadsheet. Are you sure it is present?")
        return
    title = bill.iloc[0].Short_title

    emotes = {
        "✅": "AYE",
        "abstain": "ABS"
    }

    # Fetch the votes from the reactions of the message
    votes = {}
    for reaction in message.reactions:
        emote = f"{reaction.emoji.require_colons}:{reaction.emoji.id}" if reaction.emoji.id is not None else urllib.parse.quote(reaction.emoji.name)
        users = await ctx.client.get_reactions_of_emoji(message.channel_id, message.id, emote)
        for user in users:
            votes[user['id']] = emotes.get(reaction.emoji.name, reaction.emoji.name.upper())

    # Fetch the member and add the votes
    members = data.get_members(House.LORDS if is_lords else House.COMMONS)
    members.rename(columns = {'MP':'Name', 'Peer':'Name'}, inplace = True)
    members['votes'] = members.apply(
        lambda row: "" if not row.Name else "N/A" if not row.ID else votes.pop(row.ID, "DNV"),
        axis=1
    )

    # Add the speakers vote
    vote_counts = members.groupby(['votes']).size().to_dict()
    if vote_counts.get("AYE", 0) == vote_counts.get("NO", 0):
        if division == "2nd":
            members.at[0, 'votes'] = "AYE"
        else:
            members.at[0, 'votes'] = "NO"
    else:
        members.at[0, 'votes'] = "N/A"

    # Convert the fields if the division is in the HoL
    if is_lords:
        members['votes'].replace("AYE", "CON", inplace=True),
        members['votes'].replace("ABS", "PRE", inplace=True)
        members['votes'].replace("NO", "NOT", inplace=True)
    
    # Add data to the spreadsheet
    data.add_votes(
        House.LORDS if is_lords else House.COMMONS,
        id,
        division,
        members['votes'].tolist(),
        message.author.username
    )

    aye = f"The contents," if is_lords else "The ayes to the right,"
    no = f"The not contents," if is_lords else "The noes to the left,"
    abs = f"The presents," if is_lords else "Abstentions,"

    resp = [ f"**{id.upper()}: {title}**" ]
    if vote_counts.get('AYE', 0) > 0:
        resp.append(f"{aye} {vote_counts.get('AYE')}!")
    if vote_counts.get('NO', 0) > 0:
        resp.append(f"{no} {vote_counts.get('NO')}!")
    if vote_counts.get('ABS', 0) > 0:
        resp.append(f"{abs} {vote_counts.get('ABS')}!")
    if vote_counts.get('DNV', 0) > 0:
        resp.append(f"Did not vote, {vote_counts.get('DNV')}!")

    await ctx.send("\n".join(resp))


@bot.command()
async def reload(ctx: interactions.CommandContext):
    data = XOMBPData()
    
    await ctx.send("Discarded XOMBP spreadsheet data 👍")



async def get_message(ctx: interactions.CommandContext, channel_name, id) -> interactions.Message | None:
    channel = discord.utils.get(ctx.guild.channels, name=channel_name)
    try:
        msg = await channel.get_message(id)
        return msg
    except:
        return None

bot.start()
