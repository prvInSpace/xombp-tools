#!/usr/bin/env python3

from dataclasses import dataclass
import sys
from typing import Tuple
import markdown
import re
import neocities
import argparse
from helper import get_date_string
from webhooks import send_msg_to_webhook
import xombp
from xombp import XOMBPData

bill_introduction = re.compile(".*Be it enacted by the Queen's most Excellent Majesty.*", re.IGNORECASE)
SUP = re.compile(r"(?<=[1-3])(st|nd|rd|th)(?= )")
LONG_TITLE = re.compile(r"^A Bill to", re.IGNORECASE)

ENACTING_FORMULAS = {
    "generic": """
<span class="smallcaps">Be it enacted</span> by the King's most Excellent Majesty, by and with the advice and consent of
the Lords Spiritual and Temporal, and Commons, in this present Parliament assembled, and by
the authority of the same, as follows
""",
    "finance": """
We Your Majesty's most dutiful and loyal subjects the Commons of the United Kingdom in Parliament
assembled, towards raising the necessary supplies to defray Your Majesty's public expenses and making
an addition to the public revenue have freely and voluntarily resolved to give and grant unto
Your Majesty the several duties hereinafter mentioned and do therefore most humbly beseech Your Majesty
that it may be it enacted and be it enacted by the King's Most Excellent Majesty, by and with the
advice and consent of the Lords Spiritual and Temporal, and Commons, in this present Parliament
assembled, and by the authority of the same as follows
"""
}

@dataclass
class Author:
    fullname: str
    html: str
    party: str
    emote: str

def get_author(name: str):
    data = XOMBPData()
    chars = data.get_characters()
    chars = chars[chars['Name'] == name]
    if len(chars) > 1 or len(chars) == 0:
        print(chars)
        raise Exception("Found the wrong number of authors")
    char = chars.iloc[0]

    honours = ""
    if char.Player_honours or char.Honours:
        honours = f'<span class="all-sc">{char.Player_honours} {char.Honours:}</span>'.strip()

    party = f'<img class="party-icon" src="/icons/{xombp.PARTY_TO_ICON[char.Party]}.png">'
    name = f"{char.Prefix if char.Prefix else ''} <b>{char.Name}</b> {honours if honours else ''}".strip()

    if char.Titles:       
        name += ", " + SUP.sub(lambda m: f'<sup>{m.group()}</sup>', char.Titles)

    return Author(
        char['Full-name'],
        f"{party} {name}",
        char.Party,
        xombp.PARTY_TO_EMOTE[char.Party]
    )

def get_template() -> str:
    with open("res/template.html") as html:
        return html.read()

def get_components_from_md(file: str) -> Tuple[str, str, str]:
    with open(file) as inputfile:
        text = inputfile.read().strip().split("\n")
    title = text.pop(0).replace("# ", "")
    while not text[0] or text[0].isspace():
        text.pop(0)
    longtitle = text.pop(0).replace("*", "")
    text = filter(
        lambda x: not bill_introduction.match(x),
        text
    )
    return title, longtitle, markdown.markdown("\n".join(text).strip())

def main() -> None:

    parser = argparse.ArgumentParser()
    parser.add_argument("id", help="The ID of the bill to publish")
    parser.add_argument("file", help="The markdown file to publish")
    parser.add_argument("--author", "-a", required=True, help="The author of the bill")
    parser.add_argument("--force", help="Overrides the file if it exists", action='store_true')
    parser.add_argument("--dry", help="Whether to perform a dry run", action='store_true')
    parser.add_argument("--type", choices=["government", "opposition", "pmb"], required=True, help="Who submitted the bill.")
    args = parser.parse_args(sys.argv[1:])

    author = get_author(args.author)


    template = get_template()
    template = template.replace("{date}", get_date_string())
    template = template.replace("{id}", args.id.strip().upper())
    title, longtitle, content = get_components_from_md(args.file)

    title = re.sub(" 20[0-9]{2}$", "", title)
    enacting_formula = ENACTING_FORMULAS["finance" if title == "Finance Bill" else "generic"].strip().replace("\n", " ")

    if not title or not longtitle or not content:
        raise Exception("Unable to find the title, longtitle or content of the bill.")

    longtitle = LONG_TITLE.sub("A Bill to", longtitle, 1)
    print(f"Author: {author.fullname}")
    print(f"Party: {author.party}")
    print(f"Title: {title}",)
    print(f"Long-title: {longtitle}")
    bill_type = {
        "government": "on behalf of HM Government",
        "opposition": "on behalf of HM Most Loyal Opposition",
        "pmb": "as a private member's bill"
    }[args.type]
    print(f"Type: {bill_type}")
    print(f"Enacting formula: {enacting_formula}")

    template = template.replace("{title}", title)
    template = template.replace("{longtitle}", re.sub(r"^A Bill to ", "", longtitle, re.IGNORECASE).strip())
    template = template.replace("{content}", content)
    template = template.replace("{author}", author.html)
    template = template.replace("{enacting_formula}", enacting_formula)
    with open("tmp/bill.html", "w") as output:
        output.write(template) 

    if args.dry:
        return

    with open("keys/neocities.key") as keyfile:
        api_key = keyfile.read()
        nc = neocities.Neocities(api_key)

    path = f'legislation/{args.id.lower()}.html'
    if nc.file_exists(path, path="legislation/") and not args.force:
        print("File already exists!")
        exit(0)
    
    nc.upload(path, 'tmp/bill.html')

    xombp = XOMBPData()
    xombp.add_bill(args.id, title, args.author, author.party)
    print("Bill added to spreadsheet")

    emote = "<:commons:560234593123434512>" if args.id.startswith("C") else "<:lords:560234593618231297>"
    send_msg_to_webhook(
        url="https://discord.com/api/webhooks/1017144820201955379/zGTMki4Ux9nk8hvm83Vh3FdfgOhnvcN859NsmA2CJLDNV9tDOgDjVog9FI0gqgbayFmr",
        msg=f"""
{emote} **{args.id}: {title}**
*{longtitle}*

https://xombp.neocities.org/legislation/{args.id.lower()}.html

{author.emote} **This bill was submitted by {re.sub('^The ', 'the ', author.fullname)} {bill_type}**""".strip()
    )


if __name__ == "__main__":
    main()