# Referendum on European Union Membership Bill

*A bill to hold a referendum on the United Kingdom's place within the European Union.*

## 1 Referendum
1. A referendum is to be held on whether the United Kingdom shall retain membership with the European Union.
2. The referendum is to be held on a date chosen by the Secretary of State, which is no later than six months following the enactment of this act.
3. The ballot shall ask the following question, with responses, as follows:
> "Should the United Kingdom remain a member of the European Union or leave the European Union?"
> * "Remain a member of the European Union"
> * "Leave the European Union"
3. Ballots should be made available in all the languages listed as official languages in the Languages of the UK Act 2020.
4. Those entitled to vote in the referendum are any individual who, on the date of the referendum, is eligible to vote as electors at a parliamentary election;
5. The Secretary of State may by regulations make provisions which they consider appropriate as required to fulfil the requirements of section.
6. Regulations under this Section are subject to the negative resolution procedure.
   
## 2 Protocol following the referendum
1. If the majority of ballots are in favour of leaving the European Union, the Government is required to trigger Article 50 of the Treaty on European Union before a period of two weeks following the announcement of the results.
2. The Government shall to the best of its ability negotiate a withdrawal agreement which reflects the outcome of the referendum.

## 3 Extent, commencement, and short title
1. This act may be cited as the Referendum on European Union Membership Act 2022.
2. This act comes into force upon receiving royal assent.
3. This act extends to the whole of the United Kingdom.
