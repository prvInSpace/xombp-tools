# Minimum Wage Reform (Lower Bound) Bill

*A Bill to introduce a lower bound for the minimum wage, to ensure that future minimum wage rises adheres to that lower bound, and for connected purposes.*

## 1 Amendments to the Minimum Wage Reform Act 2021
1. The Minimum Wage Reform Act 2021 is amended as set out in this section.
2. In Section 3, add new subsection (6) that reads-
> No minimum wage may be set by the Bank of England or Parliament that is below 75% of the average pay in the United Kingdom.
3. In Section 4, add new subsection (3) that reads-
> If the new minimum wage falls below 75% of average pay in the United Kingdom, it shall be increased to 75% of the average pay in the United Kingdom.

## 2 Extent, commencement, and short title
1. This Act may be cited as the Minimum Wage Reform (Lower Bound) Act 2022
2. This Act extends to the United Kingdom of Great Britain and Northern Ireland
3. This Act comes into force upon Royal Assent.