# Third Educational Reform Bill

*A Bill to reduce university fees, provide a comprehensive scholarship programme for those at the most disadvantage, as well as other reforms designed to improve mental health, bolster the community and reduce poverty.*

## 1 General Reduction in Tuition Fees
1. Universities and technical colleagues may now only charge a maximum of £6,250 for undergraduate study if they are TEF Gold or £6,000 if they are TEF Silver or Gold. HM Government will re-imburse the university with £3,000 per undergraduate.
2. This change will take effect from the academic year beginning September 2022.

## 2 Scholarship and Maintenance Grants
1. A newly created Office for Student Scholarship shall be established. HM Government is instructed to co-ordinate with other endowed scholarship funds in the establishment of this fund.
2. The OSS is instructed to accept applications for full scholarships with testimonials from pupils and schools. The OSS is also instructed to target granting scholarships to at least one third of students, and full grants for accommodation and utilities to at least a fifth.
3. In determining whether a student is eligible for a scholarship, the OSS is instructed to consider:
   1. The financial means of the student and their family, including whether the prospective student was a recipient of free school meals or their families a recipient of universal credit
   2. The location, average results and deprivation profile of the school of origin for the pupil
   3. Whether the pupil comes from a location or a family or ethnic background in which applications to top universities are not common
   4. Any accentuating circumstances, including bereavement and mental health
   5. Any achievements of exceptional civic or educational note
4. HM Government will resolve to provide at least 150,000 of these per annum.

## 3 Abolition of KS1 SATS
1. From Summer 2022, KS1 SATs are hereby abolished.

## 4 Mental Health within the Educational System
1. Every school with more than 200 pupils must employ at least one professional counsellor or mental health professional starting the academic year 2022/3 onward. HM Government will provide all necessary funds for this aim.
2. OFSTED and the ISI are instructed to assess the impact of the school’s policy on mental health and wellbeing of the pupils as a key metric in their determinations.

## 5 Free and Nutritional School Breakfast
1. Instructs all schools to be able to provide free and nutritional school breakfasts to all pupils by September 2022.
2. HM Government will provide all necessary funds for equipment and provisions.
3. Encourages schools to use these meals as a way of laying down good and healthy eating habits.

## 6 Commencement and Extent
1. This Act applies to England only.
2. This Act commences upon royal assent, although some provisions may not commence until later.