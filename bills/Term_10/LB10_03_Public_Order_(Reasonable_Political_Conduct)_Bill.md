# Public Order (Reasonable Political Conduct) Bill

*A Bill to remove limitations on the rights of individuals to reasonable political conduct, to remove the felony for republicanism, and for connected purposes.*

## 1 Amendment of the Public Order Act 1986
1. The Public Order Act 1986 is amended as described in this section.
2. In Section 6, insert new subsection (6), (7), and (8) as follows-
> (6) An offence is not committed under this section if the words or behaviour used were reasonable political conduct.
> (7) For the purposes of subsection (6), reasonable political conduct are any words or behaviours that-
>   (a) is not an offence under section (3) of this Act,
>   (b) is not threatening or abusive the person due to any protected characteristics as defined by the Equality Act 2010, and
>   (c) can be reasonably argued to be politically motivated.
> (8) The prosecution has to prove that an arrest made under this section does not violate the accused's right to reasonable political conduct. 
3. Section 5 is repealed
4. Section 6, subsection (4) is repealed
5. In Section 7, for the words "5" there is substituted "4A".

## 2 Amendment of the Treason Felony Act 1848
1. The Treason Felony Act 1848 is amended as described in this section.
2. In Section 3 omit "to deprive or depose our Most Gracious Lady the Queen, from the style, honour, or royal name of the imperial crown of the United Kingdom, or of any other of her Majesty’s dominions and countries, or"
3. In Section 3 omit "compass, imagine, invent, devise, or"
4. In Section 3 omit for the words "compassings, imaginations, inventions, devices, or intentions, or any of them," there is substituted "intentions"

## 5 Short title, commencement and extent

1. This Act may be cited as the Public Order (Reasonable Political Conduct) Act 2022.
2. This Act comes into force upon receiving royal assent.
3. This Act extends to the United Kingdom.